This page encompasses all the necessary links to documentation for the hardware development of the Helmholtz Cage, which is crucial for testing the ADCS (Attitude Determination and Control System) of the CubeSat sub-system.

* [Requirements](Requirements.md)

## Mechanical
* [Helmholtz cage mechanical design](Mechanical-design.md)
* [Control box design](Control-box-design.md)


## Electronics
* [Electronics Design Components Description](Electronics-components-description.md)
* [Failures assessment](Failures-assessment.md)
* [PCB design](PCB-design.md)