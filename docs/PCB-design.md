[TOC]

# PCB design
---

## Schematic

The PCB incorporates the following components:

- NUCLEO-L476RG
- I2C connection to magnetometer(no footprint needed for the magnetometer)
- 3 [DRV8871](https://www.ti.com/lit/ds/symlink/drv8871-q1.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1684328419701&ref_url=https%253A%252F%252Fgr.mouser.com%252F)
- 1 ON/OFF major latching switch
- Power Supply connection
- 3 Fan/s for the DRV8871
- Current sensor SMA connection

### 1. DRV8871

Schematic oh H-bridge done based on [Adafruit DRV8871](https://cdn-learn.adafruit.com/assets/assets/000/033/665/original/adafruit_products_schem.png?1467833194) and IC [datasheet](https://www.ti.com/lit/ds/symlink/drv8871-q1.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1684328419701&ref_url=https%253A%252F%252Fgr.mouser.com%252F).

Current limit: 2A per pair coil.


#### Design Requirements

| DESIGN PARAMETER           | REFERENCE | VALUE  |
|----------------------------|-----------|--------|
| Helmholtz Cage voltage     | V_M       | 48V    |
| Helmholtz Cage RMS current | I_RMS     | ~0.8 A |
| Helmholtz Cage MAX current | I_START   | 2 A    |
| ILIM resistance            | R_ILIM    | 30 kΩ  |
| PWM frequency              | f_PWM     | 5 kHz  |



<p align="center">
  <img src="uploads/DRV_scematic.png" width="400" />
</p>


### NUCLEO-L476RG

#### NUCLEO FOOTPRINT

NUCLEO-L476RG is not available as a symbol in KiCAD libraries, however a symbol of  NUCLEO64-F411RE is provided, and according to the [STM32 Nucleo-64 boards user manual](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi32uyv4YP_AhVSRPEDHbovC_4QFnoECBQQAQ&url=https%3A%2F%2Fwww.st.com%2Fresource%2Fen%2Fuser_manual%2Fum1724-stm32-nucleo64-boards-mb1136-stmicroelectronics.pdf&usg=AOvVaw34MlYBEEaYllhNpOzFjjEH) they have the same layout.

#### NUCLEO PWM connections 

<p align="center">
  <img src="uploads/nucleo.png" width="400" />
</p>

According to [this ST wiki page](https://os.mbed.com/teams/ST/wiki/pinout_labels): 

##### PWMx/y pins

The first number x represents theTimer number, the second number y represents the Channel number. 

> Example: PWM2/3 means that you use the channel 3 of Timer 2.

The letter N after the second number y means that it is the complementary of channel y (the channel is inverted). 

> Example: PWM16/1N means that you use the complementary signal of Timer 16 Channel 1.

A normal channel and its complementary channel will have the same frequency and the same duty cycle. Only their waveform will be inverted. 

> Example: PWM1/1 and PWM1/1N

All the PWMx pins have the same frequency as they use the same Timer. 

> Example: PWM2/1 and PWM2/3 will have the same frequency as they both use the Timer 2. But a different duty-cycle can be output on these two pins because they are on two different channels. If you want different frequencies you must used for example PWM1/1 and PWM2/1 instead.


Thus, we should use the same timer, for example PWMx/1 and PWMx/2 pins to generate PWM signals with the same frequency for IN1 and IN2 of every H-bridge.

| H-bridge #   | Channel name | Pin  |
|--------------|--------------|------|
| 1st H-bridge | PWM1/1       | PA_8 |
|              | PWM1/2       | PA_9 |
| 2nd H-bridge | PWM2/1       | PA_5 |
|              | PWM2/1       | PB_3 |
| 3rd H-bridge | PWM3/1       | PB_4 |
|              | PWM3/1       | PB_5 |


### I2C MAGNETOMETER CONNECTIONS
I2C channel used: I2C1
Pins: PB8 SCL , PB9 SDA

> One should check the connectors' orientation before routing

<p align="center">
  <img src="uploads/i2c.png" width="400" />
</p>

Included 4.7kOhm resistors across SDA and 3.3V and SCL and 3.3V.


### FANS

Characteristics:
- Voltage: 5VDC
- Current: 0.14A
- Connector: 2 Pins JST XH Connector
- Size: 40x40x10mm


A 1N4007 diode is used in th schematic to prevent large current spikes when the switch turns off with current still flowing through the fan inductance. A diode in this configuration is called a fly-back diode. The inductance of the fan means the current can not stop immediately when the switch turns off. The diode provides an alternate path for the current to go as it decays.

---



## Routing and Placement 

### 1. Board description and specs

- 4 layer board
- printed on 1oz copperusing JLC7628 stack up from JLCPCB 
- 1.6mm thick
- Trace width will be calculated with 60°C as ambient temperature and an allowed temperature rise of 20°C 

#### Trace width calculation

The traces where calculated using KICAD Calculator Tools

| Current | Temperature rise(ΔΤ) | Thickness       | External Traces | Internal Traces |
|---------|----------------------|-----------------|-----------------|-----------------|
| 4.5 A   | 20 C                 | 2 oz (0.07 mm)  | 0,785126 mm     | 2,04246 mm      |
| 0.6     | 20 C                 | 2 oz (0.07 mm)  | 0,0487481 mm    | 0,126815 mm     |
|         |                      |                 |                 |                 |
| 4.5 A   | 20 C                 | 1 oz (0.035 mm) | 1,5702 mm       | 4,0849 mm       |
| 0.6     | 20 C                 | 1 oz (0.035 mm) | 0,097496 mm     | 0,253631 mm     |

### 2. Inputs - Outputs

**Signal Inputs:**

* 3 single ended analog inputs
* 48 V power supply (for the coils) (1.5X3 A required MAX)
* 5 V USB power supply (for the fans) (0.6A required MAX)

**Outputs :**
* 6 PWM pins for coils control

**Communication:**

* 1 I2c port (SCL-SDA-3.3V-GND-DRDY)

**Power system capabilities :**

* 48 V @ 4.7A continuous draw
* 5V @ 2A continuous draw (based on power adapter used)


### 3. Layer stackup

- All the layers contain a ground pour due to the fact that:
    - At the top layer there are multiple SMD components and a ground plane lowers the needed traces number
    - At the internal and back layers, the ground plane is used for heat sinking reasons
- The analog section of the board is kept as further apart as possible from the digital section.
- A solid GND plane on In1 to provide a good return path for the analog signals (The plane is interrupted only at one point in order to separate the digital and analog signals)
- Added keep out areas on fans' holes sized for a M3 spacer. Furthermore, plastic spacers are going to be used in order to prevent damaging the ground filled zones with metal parts.

### 4. Analog signal routing

-   **Physical component separation** : Analog section is in its own space on the PCB, as far as possible from the digital section.
-   **Good grounding** : The board should have a solid ground plane below the analog signals in order to provide a low impedance return path, except for one point
-   **Proper spacing between traces**

### 5. EMI and noise

Added a significant number of vias connected to GND to serve as a technique for EMI (Electromagnetic Interference) shielding

### 6. DRV8871DDA Routing

Some guidlines from the datasheet:

- The bulk capacitor was placed to minimize the distance of the high-current path through the motor driver device.
- The connecting metal trace widths are as wide as possible, and numerous vias are used
when connecting PCB layers. These practices minimize inductance and allow the bulk capacitor to deliver high
current.
- Small-value capacitors are ceramic, and placed closely to device pins.
- The high-current device outputs use wide metal traces.
- The device thermal pad are soldered to the PCB top-layer ground plane. 
- Multiple vias are used to connect to a large bottom-layer ground plane. The use of large metal planes and multiple vias help dissipate the heat that is generated in the device.


Recommended layout and component placement recommendation from the datasheet is presented below:

<p align="center">
  <img src="uploads/DRV-routing.png" width="400" />
</p>


> The DRV8871-Q1 device has thermal shutdown (TSD). If the die temperature exceeds approximately 175°C, the device is disabled until the temperature drops below the temperature hysteresis level.

#### Heatsinking

The PowerPAD package uses an exposed pad to remove heat from the device. For proper operation, 
- this pad must be thermally connected to copper on the PCB to dissipate heat by adding a number of vias between the thermal pad and the ground plane
- We added 3 fans above each IC for cooling


## Assembled model

The PCB design files can be found [here](https://gitlab.com/librespacefoundation/helmholtz-coils-hw/-/tree/master/PCB/Helmholtz-Cage-PCB?ref_type=heads) and the final assembled model is shown in the pictures below:


<p align="center">
  <img src="uploads/pcb.png" width="400" />
  <img src="uploads/debug-pcb.png" width="400" />
</p>


